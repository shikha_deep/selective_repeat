#include <string>
#include "Declarations.h"

class Packet {
  public:
    PacketType type; //Type of packet
    int sequence_number; //Sequence number of the packet
    int sizeOfData; //Size of Data being transferred
    char data[1024]; //Pointer to the data being transferred


    Packet(char * buf); //Deserializer
    Packet(PacketType type, int sequence_number, int sizeOfData, char * data); //forming the packet
    void serialize(char * data);
};
