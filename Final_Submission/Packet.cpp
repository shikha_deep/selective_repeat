#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include "Packet.h"

using namespace std;

Packet::Packet(char * buff) {
  memcpy(&(this->type), buff, sizeof(PacketType));
  memcpy(&(this->sequence_number), buff + SEQNO_LOC, sizeof(int));
  memcpy(&(this->sizeOfData), buff + DATA_SIZE_LOC, sizeof(int));
  memcpy(&(this->data), buff + DATA_LOC, DATA_SIZE);
}

Packet::Packet( PacketType a, int sequence_number, int sizeOfData, char * data) {
  this->type = a;
  this->sequence_number = sequence_number;
  this->sizeOfData = sizeOfData;
  memcpy(this->data, data, sizeOfData);
}

void  Packet::serialize(char* store) {
  memcpy(store, &(this->type), sizeof(PacketType));
  memcpy(store + SEQNO_LOC, &(this->sequence_number), sizeof(int));
  memcpy(store + DATA_SIZE_LOC, &(this->sizeOfData), sizeof(int));
  memcpy(store + DATA_LOC, &(this->data), DATA_SIZE);
}
