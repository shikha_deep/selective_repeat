#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <fcntl.h>
#include <pthread.h>
#include <ctime>
#include <queue>
#include "Packet.h"

using namespace std;

typedef enum{
  SEND,
  RESEND
} Status;

struct PacketTimer{
  Packet *pack;
  Status s;
  int timestamp;
};

queue<int> resend_queue;

typedef struct {
  map<int, PacketTimer> data_map;
  pthread_mutex_t mutex; // needed to add/remove data from the buffer
  pthread_cond_t packet_ackd; // signaled when items are removed
  pthread_cond_t packet_sent; // signaled when items are added
} buffer_t;

struct send_packet_args {
  char *filename;
  int sock_send;
  const struct sockaddr* socket_type;
  buffer_t* buffer;
};

void send_packet(PacketType type, int seq_no, int data_size, char * data, int sock, const struct sockaddr* socket_type);
Packet recv_packet(int sock, struct sockaddr* socket_type);
void* filesend(void *a);
bool isExpired(int start, int stop);
void * timerThread (void * arg);
// void* send_thread(void *send_arg);

struct sockaddr_in server_send, server_ack, server_recv;
struct timeval read_timeout;
struct timeval reset_timeout;
const int length_of_sockaddr_in = sizeof(struct sockaddr_in);
unsigned int length = sizeof(struct sockaddr_in);

char filename[1024];

int main (int argc, char *argv[]) {
  if (argc < 2) {
    cout << "ERROR, no port provided\n";
    exit(0);
  }

  int server_sock_send = socket(AF_INET, SOCK_DGRAM, 0); // socket creation

  if (server_sock_send < 0) {
    cout << "ERROR, no socket created for sending." << endl;
  }

  //data socket assignemt
  bzero(&server_send, length_of_sockaddr_in);
  server_send.sin_family=AF_INET;
  server_send.sin_addr.s_addr=INADDR_ANY;
  server_send.sin_port=htons(atoi(argv[1]));

  // binding of the socket
  if (bind(server_sock_send, (struct sockaddr *) &server_send, length_of_sockaddr_in) < 0) {
    cout << "ERROR, binding failed for data socket." << endl;
  }

  // init handshake
  Packet packet = recv_packet(server_sock_send, (struct sockaddr*) &server_send);
  cout << packet.data << endl;
  char data[DATA_SIZE];
  sprintf(data, "%s\n",  "Connect ack from server --> client");
  send_packet(CONNECTION_ACK, 0, DATA_SIZE, data, server_sock_send, (const struct sockaddr*) &server_send);
  packet = recv_packet(server_sock_send, (struct sockaddr*) &server_send);
  cout << packet.data << endl;
  // handshake complete

  // socket recvfrom
  int server_sock_ack = socket(AF_INET, SOCK_DGRAM, 0); // socket creation
  if (server_sock_ack < 0) {
    cout << "ERROR, no socket created for sending." << endl;
  }

  bzero(&server_ack, length_of_sockaddr_in);
  server_ack.sin_family=AF_INET;
  char *addr = inet_ntoa(server_send.sin_addr);
  server_ack.sin_port = htons(atoi(argv[1])+1);
  int host_value = inet_pton(AF_INET, addr, &(server_ack.sin_addr));
  cout << host_value << " host_value" << '\n';

  // handshake for ack socket
  bzero(data, DATA_SIZE);
  sprintf(data, "%s\n",  "Connect test with new socket");
  send_packet(CONNECTION_REQUEST, 0, DATA_SIZE, data, server_sock_ack, (const struct sockaddr*) &server_ack);
  // handshake completed for ack socket complete

  // receiving file name and passsing it as an argument to read the file.
  //if (recvfrom(server_sock_ack, filename, DATA_SIZE, 0, (struct sockaddr *) &server_ack, (unsigned int *) &length_of_sockaddr_in) < 0) {
  if (recvfrom(server_sock_ack, filename, DATA_SIZE, 0, (struct sockaddr *) &server_recv, &length) < 0) {
    cout << "Error, Receiving file name" << endl;
    return 1;
  } else {
    cout << "Received file name is : " << filename << endl;
  }

  // check the number of packets required
  ifstream file_to_read;
  file_to_read.open(filename);
  file_to_read.seekg(0, file_to_read.end);
  int count_of_packets = file_to_read.tellg() / DATA_SIZE;
  cout <<"Number of packets required are: "<< count_of_packets << endl;

  buffer_t buffer = {};
  buffer.mutex = PTHREAD_MUTEX_INITIALIZER;
  buffer.packet_sent = PTHREAD_COND_INITIALIZER;
  buffer.packet_ackd = PTHREAD_COND_INITIALIZER;

  // start sending the file
  pthread_t t1, timeThread; // to pass the objects to client
  struct send_packet_args s1 = { filename, server_sock_send, (const struct sockaddr*) &server_send, &buffer };
  pthread_create(&timeThread, NULL, timerThread, &s1);
  pthread_create(&t1, NULL, filesend, &s1);
  // temp solution to get the number of packets

  while(1) {
    pthread_mutex_lock(&buffer.mutex);

    if(buffer.data_map.size() == 0) { // empty
      // wait until some elements are sent
      pthread_cond_wait(&buffer.packet_sent, &buffer.mutex);
    }

    Packet packet = recv_packet(server_sock_ack, (struct sockaddr*) &server_recv);
    buffer.data_map.erase(packet.sequence_number);

    // signal the fact that new items may be consumed
    pthread_cond_broadcast(&buffer.packet_ackd);
    pthread_mutex_unlock(&buffer.mutex);

    if (packet.sequence_number == count_of_packets) {
      break;
    }
  }

  pthread_join(t1, NULL);
  close(server_sock_send);
  close(server_sock_ack);
  return 0;
}

void send_packet(PacketType type, int seq_no, int data_size, char * data, int sock, const struct sockaddr* socket_type) {
  char temp[PACKET_SIZE];
  Packet packet = Packet(type, seq_no, data_size, data);
  packet.serialize(temp);

  if (sendto(sock, temp, PACKET_SIZE, 0, socket_type, length_of_sockaddr_in) < 0) {
    cout << "ERROR, Packet Sending Error." << endl;
  }
}

Packet recv_packet(int sock, struct sockaddr* socket_type) {
  char buf[PACKET_SIZE];
  unsigned int temp = length_of_sockaddr_in;
  if (recvfrom(sock, buf, PACKET_SIZE, 0, socket_type, &temp) < 0) {
    cout << "ERROR, receiving error from client side" << endl;
  }
  return Packet(buf);
}

void* filesend(void *a) {
  struct send_packet_args *args = (struct send_packet_args *)a;
  // char *filename, int sock_send, int length
  int seq_packet = 0;
  char buf[DATA_SIZE];
  struct PacketTimer pt;
  bzero(buf, DATA_SIZE);
  ifstream file_to_read;
  file_to_read.open(args->filename);

  if(!file_to_read) {
    cout << "Failed to open the specified file. " << endl;
  } else {
    while(1) {
      pthread_mutex_lock(&args->buffer->mutex);
      // cout << args->buffer->data_map.size() << " queue size" << endl;

      if(args->buffer->data_map.size() == BUF_SIZE) { // full
        // wait until some elements are consumed
        pthread_cond_wait(&args->buffer->packet_ackd, &args->buffer->mutex);
      }

      bzero(buf, DATA_SIZE);

      file_to_read.read(buf, DATA_SIZE);
      int d_size = file_to_read.gcount();
      cout << d_size << " chars read from file." << endl;
      // creating packet and packet.timer for passing to hash map.
      Packet packet = Packet(DATA, 0, DATA_SIZE,buf);

      pt.pack = &packet;
      pt.s = RESEND;
      pt.timestamp = clock();
      args->buffer->data_map.insert(pair<int, PacketTimer>(seq_packet, pt));

      if (!file_to_read) {
        send_packet(END_OF_FILE, seq_packet, d_size, buf, args->sock_send, args->socket_type);
        // signal the fact that new items may be consumed
        pthread_cond_broadcast(&args->buffer->packet_sent);
        pthread_mutex_unlock(&args->buffer->mutex);
        break;
      } else {
        send_packet(DATA, seq_packet, d_size, buf, args->sock_send, args->socket_type);
      }

      // signal the fact that new items may be consumed
      pthread_cond_broadcast(&args->buffer->packet_sent);
      pthread_mutex_unlock(&args->buffer->mutex);

      pthread_mutex_lock(&args->buffer->mutex);
      cout << "resend queue size: " << resend_queue.size() << endl;
      if (resend_queue.size() > 0 ) {
        map<int, PacketTimer>::iterator itr;
        while(resend_queue.size() > 0) {
          itr = args->buffer->data_map.find(resend_queue.front());
          Packet *p = itr->second.pack;
          cout << p->sizeOfData << endl;
          send_packet(DATA, seq_packet, p->sizeOfData, p->data, args->sock_send, args->socket_type);
          resend_queue.pop();
        }
      }
      pthread_mutex_unlock(&args->buffer->mutex);
      seq_packet++;
    };
  }

  cout << "Done with the client with "<< seq_packet << " sends" << endl;
  file_to_read.close();
  return NULL;
}

bool isExpired(int start, int stop) {
  double dur = (stop - start)/ double(CLOCKS_PER_SEC)*1000;
  cout<<"*** In isExpired. dur: "<< dur <<endl;
  return (dur > TIMEOUT) ? true : false ;
}

void* timerThread (void* arg) {
  struct send_packet_args* bufvalues = (struct send_packet_args*) arg;
  while(1) {
    usleep(10000000);
    int curr_time = clock();
    pthread_mutex_lock(&bufvalues->buffer->mutex);
    map<int, PacketTimer>::iterator it = bufvalues->buffer->data_map.begin(); //woll resemblw
    while (it != bufvalues->buffer->data_map.end()) {
      if (isExpired(it->second.timestamp , curr_time)) {
        resend_queue.push(it->second.pack->sequence_number);
        it->second.s = SEND;
      }
    }
    pthread_mutex_unlock(&bufvalues->buffer->mutex);
  }
}
