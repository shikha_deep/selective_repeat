//Enum for various types of packets with PacketType as its object
typedef enum {
  CONNECTION_REQUEST,                             //Connection Request
  CONNECTION_ACK,                                 //Connection Acknowledgement
  FILE_REQUEST,                                   //File Request
  FILE_REQUEST_ACK,                               //File Request Acknowledgement
  FILE_REQ_ERROR,                                 //File Request Error
  START_DATA_TRANSFER,                            //Begin File Transfer
  DATA,                                           //Data only
  DATA_ACK,                                       //Data Acknowledgement
  CLOSE_CONNECTION,
  END_OF_FILE
} PacketType;

//Size of packet elements
#define DATA_SIZE 512
#define BUF_SIZE 5
#define SEQNO_LOC sizeof(PacketType)                //Default size of packet type
#define DATA_SIZE_LOC (SEQNO_LOC + sizeof(int))     //Default size of sequence number
#define DATA_LOC (DATA_SIZE_LOC + sizeof(int))      //Default size of data
#define TIMEOUT 100

#define PACKET_SIZE (sizeof(PacketType) + sizeof(int) + sizeof(int) + DATA_SIZE) //Default packet size
