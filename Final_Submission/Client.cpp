#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <pthread.h>
#include <queue>
#include "Packet.h"

using namespace std;

struct recieve_args {
  int sock;
  struct sockaddr* socket_type;
  int ack_sock;
  struct sockaddr* ack_socket_type;
};

typedef struct {
  pthread_mutex_t mutex; // needed to add/remove data from the buffer
  pthread_cond_t count; // signaled when items are removed
} lock_var;

class Compare {
  public:
    bool operator() (Packet a, Packet b) {
      return (a.sequence_number > b.sequence_number)? true : false;
    }
};

// Global Vars
std::ofstream outfile ("new.txt", std::ofstream::binary);
std::queue<int> ackqueue;
std::priority_queue<Packet, std::vector<Packet>, Compare> dataQueue;

char buf[DATA_SIZE];

// prototypes
void send_packet(PacketType type, int seq_no, char * data, int sock, const struct sockaddr* socket_type);
Packet recv_packet(int sock, struct sockaddr* socket_type);

void receive_file_packet(int sock, unsigned int length, int ack_sock, const struct sockaddr* socket_type);
void* receive_file(void *received_args);

struct sockaddr_in client_send, client_ack;
const int length_of_sockaddr_in = sizeof(sockaddr_in);

int main (int argc, char *argv[]) {
  if (argc < 4) {
    cout << "ERROR, 3 arguments are not provided.\n";
    exit(0);
  }

  // creating sending socket
  int client_sock_send = socket(AF_INET, SOCK_DGRAM, 0); // socket creation
  if (client_sock_send < 0) {
    cout << "ERROR, no socket created for sending." << endl;
  }
  bzero(&client_send, length_of_sockaddr_in);
  client_send.sin_family = AF_INET;
  client_send.sin_port = htons(atoi(argv[2]));
  inet_pton(AF_INET, argv[1], &(client_send.sin_addr));

  // init handshake
  char data[DATA_SIZE];
  sprintf(data, "%s\n",  "Connect req from client --> server");
  send_packet(CONNECTION_REQUEST, 0, data, client_sock_send, (const struct sockaddr*) &client_send);
  Packet packet = recv_packet(client_sock_send, (struct sockaddr*) &client_send);
  cout << packet.data << endl;
  sprintf(data, "%s\n",  "Connect ack from client --> server with increased seq number");
  send_packet(CONNECTION_ACK, 1, data, client_sock_send, (const struct sockaddr*) &client_send); // last step of three way handshake
  // handshake complete

  // creation of ack socket
  int client_sock_ack =  socket(AF_INET, SOCK_DGRAM, 0);
  if (client_sock_ack < 0) {
    cout << "ERROR, no socket created for sending." << endl;
  }
  bzero(&client_ack, length_of_sockaddr_in);
  client_ack.sin_family = AF_INET;
  client_ack.sin_port = htons(atoi(argv[2])+1);
  client_ack.sin_addr.s_addr = INADDR_ANY;

  // binding of the socket
  if (bind(client_sock_ack, (struct sockaddr *)&client_ack, length_of_sockaddr_in) < 0) {
    cout << "ERROR, binding failed for data socket." << endl;
  } else {
    cout << "Binding is completed for second socket " << endl;
  }

  // init handshake for ack socket
  recv_packet(client_sock_ack, (struct sockaddr*) &client_ack);
  // handshake for ack socket complete

  // request file, init file meta packet
  char *filename = argv[3];
  // sending the file name with its location
  if (sendto(client_sock_ack, filename, DATA_SIZE, 0, (struct sockaddr *) &client_ack, length_of_sockaddr_in) < 0) {
    cout << "Error, Sending the file name" << endl;
  } else {
    cout << filename << " Filename is passed with size  "<< DATA_SIZE << endl;
  }

  // pushing the data to packet
  pthread_t t1;
  struct recieve_args s1 = { client_sock_send, (struct sockaddr *)&client_send, client_sock_ack, (struct sockaddr*) &client_ack };
  pthread_create(&t1, NULL, receive_file, &s1);
  pthread_join(t1, NULL);
  // poping the data received.
  lock_var var;
  cout << "Entering the poping section. " << endl;
  //pthread_mutex_lock(&var.mutex);
  // if(ackqueue.empty()) {
  //   pthread_cond_wait(&var.count, &var.mutex);
  // } else {
    // while(!ackqueue.empty()) {
    //   int seq_no = ackqueue.front();
    //   ackqueue.pop();
    // }
  // while(1) {
  //   pthread_mutex_lock(&var.mutex);
  //   if(ackqueue.empty()) {
  //     pthread_cond_wait(&var.count,&var.mutex);
  //   }
	// 	int seq_no = ackqueue.front();
  //   ackqueue.pop();
  //   pthread_mutex_unlock(&var.mutex);
  //   if(seq_no == -1) break;
  //   send_packet(DATA_ACK, packet.sequence_number, buf, client_sock_ack, (const struct sockaddr*) &client_ack  );
  // }
  // pthread_mutex_unlock(&var.mutex);
  close(client_sock_send);
  close(client_sock_ack);
  return 0;
}

void send_packet(PacketType type, int seq_no, char * data, int sock, const struct sockaddr* socket_type) {
  Packet packet = Packet(type, seq_no, DATA_SIZE, data);
  char temp[PACKET_SIZE];
  packet.serialize(temp);
  if (sendto(sock, temp, PACKET_SIZE, 0, socket_type, length_of_sockaddr_in) < 0) {
    cout << "ERROR, Packet Sending Error." << endl;
  }
}

Packet recv_packet(int sock, struct sockaddr* socket_type) {
  char buf[PACKET_SIZE];
  unsigned int temp = length_of_sockaddr_in;
  if (recvfrom(sock, buf, PACKET_SIZE, 0, socket_type, &temp) < 0) {
    cout << "ERROR, receiving error from client side" << endl;
  }
  return Packet(buf);
}

void receive_file_packet(int sock, struct sockaddr* sock_type, int ack_sock, struct sockaddr* ack_sock_type) {
  // int y = 0;
  while(1) {
    Packet packet = recv_packet(sock, sock_type);
    lock_var var;
    bzero(buf, DATA_SIZE);

    pthread_mutex_lock(&var.mutex);
    ackqueue.push(packet.sequence_number);

    pthread_cond_signal(&var.count);
    pthread_mutex_unlock(&var.mutex);

    dataQueue.push(packet);
    outfile.write(packet.data, packet.sizeOfData);
    dataQueue.pop();

    // if( y++ == 7) {
    //   usleep(60000000);
    //   cout << "time is up" << endl;
    // }

    // cout << packet.sequence_number << " receivedfilae_packet function " << endl;
    sprintf(buf, "%d\n", packet.sequence_number);

    send_packet(DATA_ACK, packet.sequence_number, buf, ack_sock, ack_sock_type);

    if (packet.type == END_OF_FILE) {
      outfile.close();
      break;
    }
  }
}

void* receive_file(void *recv_args) {
  cout << "In thread Function" << endl;
  struct recieve_args *recv_args_struct = (struct recieve_args *) recv_args;
  receive_file_packet(recv_args_struct->sock, recv_args_struct->socket_type, recv_args_struct->ack_sock, recv_args_struct->ack_socket_type);
  return NULL;
}
